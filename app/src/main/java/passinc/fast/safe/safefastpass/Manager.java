package passinc.fast.safe.safefastpass;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.TextView;

import java.security.SecureRandom;
import java.util.ArrayList;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;

/**
 * Created by Cameron on 4/5/2016.
 */
public class Manager {


    //domm3n5
    private static final String PasswordFile = "passwords";
    private static final String SettingsFile = "settings";
    public enum Status
    {
        WrongPassword,
        Success,
        Lockout,
    }


    //Setup the Manager by passing the context of the application
    private Context mContext = null;
    public Manager(Context mContext)
    {
        this.mContext = mContext;
    }

    //If True popup to notify user/warn.
    public boolean IsDeviceRooted()
    {

        String[] paths = { "/system/app/Superuser.apk",
                "/sbin/su", "/system/bin/su", "/system/xbin/su",
                "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su",
                "/system/bin/failsafe/su", "/data/local/su" };
        for (String path : paths) {
            if (new File(path).exists()) return true;
        }
        return false;
    }


    private int tries = 0;
    private String CryptoPassword = "";
    public Status LoadManager(String Password)
    {
        CryptoPassword = Password;

        if(tries > 3)
        {
            return Status.Lockout;
        }

        if(LoadSettings() == false) {
            tries +=1;
            CryptoPassword = "";
            return Status.WrongPassword;
        }

        if(LoadPasswordList() == false)
        {
            tries +=1;

            CryptoPassword = "";
            return Status.WrongPassword;
        }
        if(firstLoad())
        {
            //New Loading Stuff.


        }
        else {

        }
        ManagerLoaded = true;
        return Status.Success; //or Lockout or WrongPass
    }

    public boolean isManagerLoaded()
    {
        return ManagerLoaded;
    }

    public boolean firstLoad()
    {
        if(!new File(mContext.getFilesDir(),PasswordFile).exists())
            if(!new File(mContext.getFilesDir(),SettingsFile).exists())
                return true;

        return firstLoad;
    }

    public void wipe()
    {
        if(new File(mContext.getFilesDir(),PasswordFile).exists())
            new File(mContext.getFilesDir(),PasswordFile).delete();
        if(new File(mContext.getFilesDir(),SettingsFile).exists())
            new File(mContext.getFilesDir(),SettingsFile).delete();

    }

    private boolean ManagerLoaded = false;
    public Settings GetSettings()
    {
        return mySettings;
    }
    public ArrayList<Password> GetPasswordList()
    {
        return myPasswords;
    }

    private ArrayList<Password> myPasswords;
    private Settings mySettings;

    private boolean firstLoad = false;

    public boolean LoadPasswordList()
    {
        FileInputStream  outputStream;
        if(!new File(mContext.getFilesDir(),PasswordFile).exists())
        {
            firstLoad = true;
            myPasswords = new ArrayList<Password>();
            return true;
        }
        try {
            outputStream= mContext.openFileInput(PasswordFile);

            byte[] data = read(outputStream);

            byte[] result = decrypt(CryptoPassword, data);

            ByteArrayInputStream inputStream = new ByteArrayInputStream(result);

            ObjectInputStream  oos = new ObjectInputStream (inputStream);
            myPasswords = (ArrayList<Password>) oos.readObject();
            if(myPasswords == null)
            {
                myPasswords = new ArrayList<Password>();

            }
            return true;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    //StackOverFlow Simple File Reader - no need to reinvent the wheel
    //http://stackoverflow.com/questions/858980/file-to-byte-in-java
    public byte[] read(FileInputStream file) throws IOException {

        ByteArrayOutputStream ous = null;
        try {
            byte[] buffer = new byte[4096];
            ous = new ByteArrayOutputStream();
            int read = 0;
            while ((read = file.read(buffer)) != -1) {
                ous.write(buffer, 0, read);
            }
        }finally {
            try {
                if (ous != null)
                    ous.close();
            } catch (IOException e) {
            }

            try {
                if (file != null)
                    file.close();
            } catch (IOException e) {
            }
        }
        return ous.toByteArray();
    }
    public boolean SavePasswordList(ArrayList<Password> passwords)
    {
        FileOutputStream outputStream;

        if(CryptoPassword.equals( ""))
        {
            return false;
        }

        try {
            outputStream= mContext.openFileOutput(PasswordFile, mContext.MODE_PRIVATE);

            ByteArrayOutputStream b = new ByteArrayOutputStream();


            ObjectOutputStream oos = new ObjectOutputStream(b);
            oos.writeObject(passwords);
            oos.close();

            //Convert it to Encrypted Format
            byte[] cryptoSave = encrypt(CryptoPassword,b.toByteArray());
            outputStream.write(cryptoSave);
            outputStream.close();

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }



    public boolean LoadSettings()
    {
        FileInputStream  outputStream;
        if(!new File(mContext.getFilesDir(),SettingsFile).exists())
        {
            firstLoad = true;
            mySettings = new Settings();
            return true;
        }

        try {
            outputStream= mContext.openFileInput(SettingsFile);
            ObjectInputStream  oos = new ObjectInputStream (outputStream);
            mySettings = (Settings) oos.readObject();
            return true;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean SaveSettings(Settings settings)
    {
        if(CryptoPassword.equals( ""))
        {
            return false;
        }
        FileOutputStream outputStream;

        try {
            outputStream= mContext.openFileOutput(SettingsFile, mContext.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(outputStream);
            oos.writeObject(settings);
            oos.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }


    //Generate 128Bit Key
    private static byte[]  getRawKeyFromString(String myKey) throws Exception
    {
        byte[] keyStart = myKey.getBytes();
        KeyGenerator kgen = KeyGenerator.getInstance("AES");
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        sr.setSeed(keyStart);
        kgen.init(128, sr); //128 bits encryption
        SecretKey skey = kgen.generateKey();
        byte[] key = skey.getEncoded();
byte[] finalkey = new byte[16];
        for(int i =0; i < keyStart.length % 16; i ++)
            finalkey[i] = keyStart[i];
        //SecretKeySpec secretKeySpec = new SecretKeySpec(myKey, "AES");
        return finalkey;
    }

    //Use a Key to Encrypt Data
    private static byte[] encrypt(String key, byte[] data) throws Exception
    {
        SecretKeySpec skeySpec = new SecretKeySpec(getRawKeyFromString(key), "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
        byte[] encrypted = cipher.doFinal(data);

        return encrypted;
    }

    //Use a Key to Decrypt Data
    private static byte[] decrypt(String key, byte[] encryptedData) throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(getRawKeyFromString(key), "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, skeySpec);
        byte[] decrypted = cipher.doFinal(encryptedData);
        return decrypted;
    }
}
