package passinc.fast.safe.safefastpass;

import android.content.Context;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class PasswordListAdapter extends BaseAdapter {

    private ArrayList<Password> mItemsFiltered = null;
    private ArrayList<Password> mItems = null;
    private final Context mContext;

    private static final String TAG = "AdapterPasswords";

    public PasswordListAdapter(Context context, ArrayList<Password> myPasswords) {

        mItems = myPasswords;

        mItemsFiltered = new ArrayList<Password>();
        mItemsFiltered.addAll(myPasswords);


        mContext = context;

    }

    private String currentFilter = "";
    public void FilterByString(String filter)
    {
        filter = filter.toLowerCase();
        this.currentFilter = filter;
        mItemsFiltered = new ArrayList<Password>();


        for(int i =0; i < mItems.size(); i ++)
        {

            if(filter == "" || mItems.get(i).getService().toLowerCase().contains(filter)
                    )
                mItemsFiltered.add(mItems.get(i));
        }

        notifyDataSetChanged();


    }

    // Add a ToDoItem to the adapter
    // Notify observers that the data set has changed

    public void add(Password item) {

        mItems.add(item);
        FilterByString(currentFilter);
        notifyDataSetChanged();

    }

    // Clears the list adapter of all items.

    public void clear() {

        mItems.clear();
        mItemsFiltered.clear();
        FilterByString(currentFilter);
        notifyDataSetChanged();

    }

    // Returns the number of ToDoItems

    @Override
    public int getCount() {

        return mItemsFiltered.size();

    }

    // Retrieve the number of ToDoItems

    @Override
    public Object getItem(int pos) {

        return mItemsFiltered.get(pos);

    }

    // Get the ID for the ToDoItem
    // In this case it's just the position

    @Override
    public long getItemId(int pos) {

        return pos;

    }

    // Create a View for the ToDoItem at specified position
    // Remember to check whether convertView holds an already allocated View
    // before created a new View.
    // Consider using the ViewHolder pattern to make scrolling more efficient
    // See: http://developer.android.com/training/improving-layouts/smooth-scrolling.html

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final Password passwordItem = mItemsFiltered.get(position);

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        RelativeLayout itemLayout = (RelativeLayout) inflater.inflate(R.layout.pass_item, null);

        final TextView serviceView = (TextView) itemLayout.findViewById(R.id.service);
        serviceView.setText(passwordItem.getService());

        final TextView passwordView = (TextView) itemLayout.findViewById(R.id.password);
        passwordView.setText(passwordItem.getPassword());

        itemLayout.findViewById(R.id.visibilityIcon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                passwordItem.toggleSelected();
                if (passwordItem.selected()) {
                    v.setAlpha(1.0f);
                    passwordView.setInputType(InputType.TYPE_CLASS_TEXT);
                } else {
                    v.setAlpha(0.3f);
                    passwordView.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
            }
        });

        // Return the View you just created
        return itemLayout;

    }
}
