package passinc.fast.safe.safefastpass;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.Date;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.support.v4.app.DialogFragment;

/**
 * Created by Admin on 4/25/2016.
 */
public class PasswordPopupDialog extends DialogFragment  {
    public interface NewPasswordGiven {
        void onNewPasswordInput(String inputText);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        final LinearLayout login = (LinearLayout)inflater.inflate(R.layout.login_dialog, null);
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(login);

        // Add action buttons
        builder.setPositiveButton(R.string.password_login, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {

                //PasswordPopupDialog.this.getDialog().

                EditText myEdit = (EditText)login.findViewById(R.id.password);
                NewPasswordGiven activity = (NewPasswordGiven) getActivity();
                activity.onNewPasswordInput(myEdit.getText().toString());

                PasswordPopupDialog.this.getDialog().dismiss();

            }
        });
        builder.setNegativeButton(R.string.password_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                PasswordPopupDialog.this.getDialog().cancel();
            }
        });
        return builder.create();
    }
}
