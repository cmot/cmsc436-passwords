package passinc.fast.safe.safefastpass;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.Date;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements PasswordPopupDialog.NewPasswordGiven {

    //This gets the Password Input from the Dialog.
    public void onNewPasswordInput(String password)
    {

        Manager.Status login = myPassManager.LoadManager(password);
        if(login == Manager.Status.Success)
            setupAdapter(password);
        else if(login == Manager.Status.Lockout)
        {
            Toast.makeText(this.getApplicationContext(),"ERROR - Locked out",Toast.LENGTH_LONG).show();
        }
        else if(login == Manager.Status.WrongPassword)
        {

            Toast.makeText(this.getApplicationContext(),"ERROR - WRONG PASSWORD",Toast.LENGTH_LONG).show();
            PasswordPopupDialog pass = new PasswordPopupDialog();
            pass.show(getSupportFragmentManager(), "Pass");
        }
    }


    PasswordListAdapter mAdapter;
    Manager myPassManager;

    private static final String TAG = "PassMainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myPassManager = new Manager(getApplicationContext());

        if(myPassManager.IsDeviceRooted())
        {
            Toast.makeText(this.getApplicationContext(),"Your Device appears to be rooted [Warning] your security cannot be assured", Toast.LENGTH_LONG).show();
        }

        FloatingActionButton myFab = (FloatingActionButton)  findViewById(R.id.fab);
        myFab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //launch "create new password"
                //we need a class that ties to the generate add_pass.xml interface
                Intent createNewPassIntent = new Intent(MainActivity.this, AddPassword.class);
                startActivityForResult(createNewPassIntent, 1);
            }
        });

        SearchView mySearch = (SearchView)findViewById(R.id.searchView);
        mySearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String arg0) {

                return true;
            }

            @Override
            public boolean onQueryTextChange(String arg0) {

                if(mAdapter != null)
                {
                    mAdapter.FilterByString(arg0);
                }
                return true;
            }
        });
    }

    //Once the User is Logged in we setup the adapter
    public void setupAdapter(String password)
    {
        if(myPassManager.isManagerLoaded() == false)
            return;


        mAdapter = new PasswordListAdapter(getApplicationContext(), myPassManager.GetPasswordList());
        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(mAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();

        if(myPassManager.isManagerLoaded() == false)
        {
          //  myPassManager.wipe();
            if(myPassManager.firstLoad())
            {
                PasswordCreatePopupDialog pass = new PasswordCreatePopupDialog();
                pass.show(getSupportFragmentManager(), "Pass");
            }
            else {
                PasswordPopupDialog pass = new PasswordPopupDialog();
                pass.show(getSupportFragmentManager(), "Pass");
            }
        }

        // Load saved ToDoItems, if necessary

        //It should load the items in the OnCreate
        //if (mAdapter.getCount() == 0)
        //    loadItems();
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Save ToDoItems

        //We Passed the Object from GetPasswordsLists to the Adapter Prior.
        myPassManager.SavePasswordList(myPassManager.GetPasswordList());
        myPassManager.SaveSettings(myPassManager.GetSettings());

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.i(TAG,"Entered onActivityResult()");

        // TODO - Check result code and request code
        // if user submitted a new ToDoItem
        // Create a new ToDoItem from the data Intent
        // and then add it to the adapter


        if(resultCode == Activity.RESULT_OK)
        {
            String pass = data.getStringExtra("password");
            String service = data.getStringExtra("service");

            mAdapter.add(new Password(pass, service));
        }





    }

}
