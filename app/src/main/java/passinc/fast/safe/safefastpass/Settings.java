package passinc.fast.safe.safefastpass;

import java.io.Serializable;

/**
 * Created by Cameron on 4/5/2016.
 */
public class Settings  implements Serializable {


    //Put the Settings Variables Here
    public int min_length;
    public int max_length;
    public int punctuation;
    public int lucky_number;
    public boolean include_spaces;

    //default settings file
    public Settings(){
        min_length = 10;
        max_length = 17;
        punctuation = 0;
        lucky_number = -1;
        include_spaces = false;
    }

    public Settings(int min, int max, int punct, int lucky_num, boolean spaces){
        min_length = min;
        max_length = max;
        punctuation = punct;
        lucky_number = lucky_num;
        include_spaces = spaces;
    }
}
