package passinc.fast.safe.safefastpass;

import java.util.HashMap;
import java.util.Random;

/**
 * Created by Cameron on 4/5/2016.
 */
public class PasswordGenerator {

    private static final int APPEND_QUESTION = 1;
    private static final int APPEND_EXCLAMATION = 2;
    private static final int APPEND_PERIOD = 3;

    private HashMap<Integer, String[]> dictionaries;
    private final String[] dict1 = {"Bar", "Car", "Jar", "Scar", "Par", "Tar","Far","Are","Tsar"};
    private final String[] dict2 = {"Cat", "Rat", "Bat", "Mat", "Nat", "Fat", "Sat", "Vat", "Hat"};
    private final String[] dict3 = {"Cop","Slop","Crop","Shop","Top","Prop","Drop", "Stop"};
    private final String[] dict4 = {"Dog", "Frog", "Bog", "Cog", "Log", "Smog", "Slog", "Hog", "Jog"};
    private final String[] dict5 = {"Bug", "Hug", "Slug", "Shrug", "Pug", "Rug", "Jug", "Mug", "Snug"};
    private final String[] dict6 = {"Bee", "Knee", "Key", "Tree", "Flea", "Tea", "Ski", "Sea", "Pea"};



    //dummy value
    public char punctuation = '#';

    public PasswordGenerator(){
        dictionaries = new HashMap<>();
        dictionaries.put(0,dict1);
        dictionaries.put(1,dict2);
        dictionaries.put(2,dict3);
        dictionaries.put(3,dict4);
        dictionaries.put(4,dict5);
        dictionaries.put(5,dict6);


    }

    // Generate a password according to settings file
    public String generatePassword(Settings settings)
    {
        if(settings.punctuation == APPEND_QUESTION){
            punctuation = '?';
        }
        else if (settings.punctuation == APPEND_EXCLAMATION){
            punctuation = '!';
        }
        else if (settings.punctuation == APPEND_PERIOD){
            punctuation = '.';
        }

        String p = buildPassword(settings);


        return p;
    }

    // Returns a string composed of two pairs of randomly chosen words
    private String buildPassword(Settings settings)
    {
        Random r = new Random();
        String result;
        String[] dictA, dictB;
        int first, second;

        first = r.nextInt(dictionaries.size());
        second = r.nextInt(dictionaries.size());
        while(second == first){
            second = r.nextInt(dictionaries.size());
        }

        dictA = dictionaries.get(first);
        dictB = dictionaries.get(second);

        int pos_first_word_a = r.nextInt(dictA.length-1);
        int pos_second_word_a = r.nextInt(dictA.length-1);
        int pos_first_word_b = r.nextInt(dictB.length-1);
        int pos_second_word_b = r.nextInt(dictB.length-1);

        while(pos_first_word_a == pos_second_word_a){
            pos_second_word_a = r.nextInt(dictA.length-1);
        }
        while(pos_first_word_b == pos_second_word_b){
            pos_second_word_b = r.nextInt(dictB.length-1);
        }

        String first_word_a = dictA[pos_first_word_a];
        String second_word_a = dictA[pos_second_word_a];
        String first_word_b = dictB[pos_first_word_b];
        String second_word_b = dictB[pos_second_word_b];

        if(settings.include_spaces == true){
            result = first_word_a + " " + first_word_b + " " + second_word_a + " " + second_word_b;
        }
        else {
            result = first_word_a + first_word_b + second_word_a + second_word_b;
        }
        if(settings.lucky_number > 0){
            int min = 1;
            int max = 99;

            Random rr = new Random();
            int i1 = rr.nextInt(max - min + 1) + min;
            result = result + i1;
        }

        if (settings.punctuation != 0){
            result = result + '!';
        }

        return result;
    }
}
