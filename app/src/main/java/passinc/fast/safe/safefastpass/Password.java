

package passinc.fast.safe.safefastpass;

import java.io.Serializable;

/**
 * Created by Cameron on 4/5/2016.
 */
public class Password implements Serializable {

    private String Password;
    private String Service;
    private Boolean Selected;

    public Password(String password, String service) {
        Password = password;
        Service = service;
        Selected = false;
    }
    public String getPassword() {
        return Password;
    }
    public String getService() {
        return Service;
    }

    public Boolean selected() {
        return Selected;
    }
    public void toggleSelected() {
        Selected = !Selected;
    }
}