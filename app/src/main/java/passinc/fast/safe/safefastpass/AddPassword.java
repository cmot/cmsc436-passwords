package passinc.fast.safe.safefastpass;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

/**
 * Created by smf7293 on 4/25/16.
 */
public class AddPassword extends Activity {

    private String service;
    private String password;
    private Settings settings;

    private PasswordGenerator passGen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_pass);
        passGen = new PasswordGenerator();
        settings = new Settings();
        password = passGen.generatePassword(settings);
        TextView passwordGUI = (TextView)findViewById(R.id.passPreview);
        passwordGUI.setText(password);

        //Listener for confirm button
        Button confirmButton = (Button) findViewById(R.id.confirmPassButton);
        confirmButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //return finished password

                Intent returnIntent = new Intent();
                EditText serviceGUI = (EditText)findViewById(R.id.passService);
                service = serviceGUI.getText().toString();

                TextView passwordGUI = (TextView)findViewById(R.id.passPreview);
                password = passwordGUI.getText().toString();

                returnIntent.putExtra("password", password);
                returnIntent.putExtra("service", service);

                setResult(Activity.RESULT_OK,returnIntent);
                finish();
            }
        });

        //Listener for refresh button
        Button refreshButton = (Button) findViewById(R.id.refreshPassButton);
        refreshButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //return finished password
                password = passGen.generatePassword(settings);
                TextView passwordGUI = (TextView)findViewById(R.id.passPreview);
                passwordGUI.setText(password);

            }
        });

        //Listener for number switch
        Switch numberSwitch = (Switch) findViewById(R.id.numberSwitch);
        numberSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                if (isChecked) {
                    //Currently uses default number of 11. UI needs updated to allow custom number
                    //Or password Generator can randomly generate a number
                    settings.lucky_number = 11;
                } else {
                    settings.lucky_number = -1;
                }
                //return finished password
                password = passGen.generatePassword(settings);
                TextView passwordGUI = (TextView)findViewById(R.id.passPreview);
                passwordGUI.setText(password);

            }
        });

        //Listener for punctuation switch
        Switch punctuationSwitch = (Switch) findViewById(R.id.punctuationSwitch);
        punctuationSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                if(isChecked){
                    //Currently just toggles exclamation mark on or off. UI can be updated to
                    //allow preference of punctuation
                    settings.punctuation = 2;
                } else{
                    settings.punctuation = 0;
                }
                //return finished password
                password = passGen.generatePassword(settings);
                TextView passwordGUI = (TextView)findViewById(R.id.passPreview);
                passwordGUI.setText(password);

            }
        });

    }
}
